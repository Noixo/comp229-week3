package com.company;

import java.awt.*;
import java.awt.Rectangle;

public class Cell extends Rectangle{    //Calls a super class and says that the subclass will
                                        // be calling from the super class

    //int x;
    //int y;            //x and y no longer needed as they are being called from the super class Rectangle

    public Cell(int x, int y) {
        super(x, y, 35, 35);    //Calls the superclass constructor in rectangle and sets the size of the cell to be x position + 35, y position + 35
        //this.x = x;
        //this.y = y;
    }

    public void paint(Graphics g, Boolean highlighted) {
        if (highlighted) {
            g.setColor(Color.LIGHT_GRAY);           //Sets colour of the rectangle area to been light gray when mouse is over it
            g.fillRect(x, y, 35, 35);       //Fills the rectangle created by the drawRect with the current position of the mouse
        }

        g.setColor(Color.BLACK);    //Sets colour of rectangle to black
        g.drawRect(x, y, 35, 35);   //Draws the physical rectangle
    }
    public boolean contains(Point p)    //Point is an object that has the x,y position of the mouse
    {
        if (p == null)      //Protects code from null error.
            return false;
        return super.contains(p);   //Super says to take a method in the Rectangle class rather than Cell and check if pointer location is currently in the box
    }
    /*
    public boolean contains(Point target){
        if (target == null)
            return false;
        return target.x > x && target.x < x + 35 && target.y > y && target.y < y +35;
    } */
}